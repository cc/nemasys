#!/bin/bash
###############
# bridge.sh
# Activate a connection profile for each USB ethernet dongle
# Launch the network bridge application with them as args
# this is CC's attempt -- who knows, maybe there's a better way
# assumes it's looking for USB network dongles
###############

# here, you need to manually list all the connection profiles
# that have been created and are going to be applied to the dongles

cpList=( 
connection192.168.2.202 
connection192.168.2.203
)
# run symmetrical impairments
app="qt/nemasys/nemasys"

###############
# temporary files that will be used below
nicsFile="/tmp/nics.txt"
nicFile="/tmp/nic.txt"
nicNumFile="/tmp/nicNum.txt"

# write list of current devices to tmp file, USB names have 'u' in them
# so only list those
nmcli device status | awk '/en/ && /u/' > $nicsFile
NUMOFNICS=$(wc -l < "$nicsFile")
  printf "there are %d connection profiles noted in bridge.sh\n" "${#cpList[@]}"
  printf "there are %d USB NIC adapters currently attached\n" "$NUMOFNICS"
if ((${#cpList[@]} != $NUMOFNICS)); then
  printf "and they probably should be the same number\n!!!!!!!!\n!!!!!!!!\n
    but, ok let's see what we get\n"
else
  printf "\n"
fi

# iterate over connection profiles listed above
COUNTER=0

for cp in "${cpList[@]}"
do
# read next whole line from nmcli device status
  read -r line

# write first item from that line to another tmp file -- name of the nic
  echo $line | awk '{print $1;}' > $nicFile

# read nic name from tmp file
  command eval  'nic=($(cat $nicFile))'

# ugly hack to map of pysical port to fixed argument position
# canonical mapping is
# PORT 1 = arg -a = enp0s20u1
# PORT 2 = arg -b = enp0s20u2
# look for 'u2' in the name
  cat $nicFile | awk '/en/ && /u2/' > $nicNumFile
# print number of lines produced (0 for enp0s20u1, 1 for enp0s20u2)
  ISPORT2=$(wc -l < "$nicNumFile")
  PORT=$(($ISPORT2 + 1))
if [ -n "$nic" ]; then
  printf "Activating connection profile %s using NIC %s\n" "$cp" "$nic" 
#  printf "NIC %s which is in USB port %s\n" "$nic" "$PORT" 
  nmcli con up $cp ifname $nic

  COUNTER=`expr $COUNTER + 1`
#   echo "COUNTER $COUNTER"

# select arg for call to app
  case "$PORT" in
  1)  app+=" -a "
      ;;
  2)  app+=" -b "
      ;;
  *) echo "messed up which NIC's are allowed as args to bridge"
     ;;
  esac  

# set $nic as arg for call to app
  app+=$nic

else
  printf "too many connection profiles, no free NIC not found for 
    connection profile %s\n" "$cp"
fi
done < "$nicsFile"
echo $app
$app

