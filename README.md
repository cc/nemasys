this is a public project on
> https://cm-gitlab.stanford.edu/public

copy a read-only snapshot with the download button

or clone it to a repository on a local machine 
> git clone git@cm-gitlab.stanford.edu:cc/nemasys.git

(the repo's origin requires permission to be modified)
****
# Network emulator asymmetrical system (Nemasys)
or
# Network Media Async Simulator (Nemasys)
Qt-based network test bench on top of NetEm
****
wire up 3 PC's using ethernet cables plugged into their NICs

host(1) &larr;&rarr; nemasys host(2) &larr;&rarr;  host(3)

right-going and left-going traffic are 
independently (asymmetrically) subject to 
impairments which are set by the nemasys GUI
****
# Install
build a linux machine which supports jacktrip build and you'll have what's
needed for a nemasys build

# fedora: 
> https://ccrma.stanford.edu/blogs/cc/bare-machine-fedora-30-or-29-jacktrip-compiled-cm-gitlab

# ubuntu:
> see jacktripBionic.txt in this project

# Build

# (with Qt Creator IDE)
click "Open Project"  
navigate to nemasys/nemasys/nemasys.pro  
click "Open"  
go to "Projects" and make sure that "Shadow build:" is unchecked  
build with < control > b or the hammer button

# (or via cmd line)
cd nemasys/nemasys  
qmake-qt5  
make  

# Run

# (cmd line)
(open a terminal in the top-level project directory)  
cd nemasys  
./bridge.sh