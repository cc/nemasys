#ifndef GLOBALS_H
#define GLOBALS_H

// the bridge has two outward-facing network interface cards, usb
// their names are now set via cmd line args

#define VERSION "1.0.0"

// switch off experiment control
// #define EXP

#define AT_GIBSON

#ifndef AT_GIBSON

// old way
#define NIC0 "enp0s20u1"
#define NIC1 "enp0s20u2"
#define MPLAYER "/home/cc/Downloads/MPlayer-1.1.1/mplayer"

#else

#define NIC0 _nic0
#define NIC1 _nic1

#endif

// not used for awecom
////////////////////////////
// what this machine uses to play media
#define MPLAYER "ecasound"
// msec between ref end and comparison clip start
#define CLIPGAP 2000
//#define LOOP_TEST

#endif // GLOBALS_H

