#ifndef DISPATCH_H
#define DISPATCH_H

#include <QObject>
#include <QThread>
#include "state.h"
#include "bridge.h"

class Dispatch : public QThread
{
    Q_OBJECT

public:
    Dispatch();
    virtual void run();
    bool loop;
    bool ref;
    State state;
    Bridge bridge;
    void setClipAndDur(QString clip, int secs);
private:
    void play();
    int msecsDur;
signals:
    void donePlaying();
};

#endif // DISPATCH_H
