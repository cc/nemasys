#ifndef STATE_H
#define STATE_H


class State
{
public:
    State();
    void print();
    void printDrift();
    void init();
    void set_delay(int i, double v);
    double get_delay(int i);
    void set_drift(int i, double v);
    double get_drift(int i);
    void set_loss(int i, double v);
    double get_loss(int i);
 private:
    double delay[2];
    double drift[2];
    double loss[2];

};

#endif // STATE_H
