#ifndef EXP_H
#define EXP_H

#include <QObject>
#include "ui_exp.h"
#include "dispatch.h"

class Trials
{
public:
    Trials(){}
    QString path;
    QStringList args;
    int nChans;
    bool video;
    bool bridge;
};

class Exp : public QObject
{
    Q_OBJECT

public:
    Exp();
    Ui::exp *ui;
    Dispatch dispatch;
    void ready();
    Trials trials;
    void showCurrentClip();
    void go();
    void trialNumChanged();
public slots:
    void setupNextPlay();
private:
    int trialNumZeroBased;
    bool ref;
};

#endif // EXP_H
