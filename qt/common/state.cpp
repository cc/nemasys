#include <QDebug>
#include "state.h"

State::State()
{
    init();
}

// init bridge state with no empairments
void State::init()
{
    for (int i = 0; i < 2; i++)
    {
        set_delay(i,0.0);
        set_drift(i,0.0);
        set_loss(i,0.0);
    }
}

void State::print()
{
    qDebug() << "bridge delay(0) "  << "\t" << "\t" << get_delay(0);
    qDebug() << "bridge jitter(0) "  << "\t" << "\t" << get_drift(0);
    qDebug() << "bridge loss(0) "  << "\t" << "\t" << get_loss(0) << "\n" << "*********";
    qDebug() << "bridge delay(1) "  << "\t" << "\t" << get_delay(1);
    qDebug() << "bridge jitter(1) "  << "\t" << "\t" << get_drift(1);
    qDebug() << "bridge loss(1) "  << "\t" << "\t" << get_loss(1) << "\n";
}

void State::set_delay(int i, double v) {delay[i] = v;}
double State::get_delay(int i) {return delay[i];}
void State::set_drift(int i, double v) {drift[i] = v;}
double State::get_drift(int i) {return drift[i];}
void State::set_loss(int i, double v) {loss[i] = v;}
double State::get_loss(int i) {return loss[i];}
