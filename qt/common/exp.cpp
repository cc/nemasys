#include "exp.h"
#include "globals.h"
#include <QtGui>

// list is zero-based but display 1-based
#define DPYTRIALNUM (trialNumZeroBased + 1)

// trial list entries have 4 fields
// clip, dur. bridge param, param value
#define TN(x) (trialNumZeroBased*4 + x)

Exp::Exp()
{
    trialNumZeroBased = -1;
    ref = false;
    // increment trial number, prepare, wait for click or go
    QObject::connect(&dispatch, SIGNAL(donePlaying()), this, SLOT(setupNextPlay()));
}

void Exp::go()
{
    dispatch.bridge.setClipPath(trials.path);
    dispatch.bridge.setBridge(trials.bridge);
    dispatch.bridge.setNChans(trials.nChans);
    dispatch.bridge.setVideo(trials.video);
    setupNextPlay(); // logic to play reference first
    showCurrentClip();
}

void Exp::trialNumChanged()
{
    if (ui->trialSpinBox->value() > (trials.args.size()/4))
#ifdef LOOP_TEST
        ui->trialSpinBox->setValue(1);
#else
        qApp->quit();
#endif
    else showCurrentClip();
}

void Exp::showCurrentClip()
{
    trialNumZeroBased = ui->trialSpinBox->value()-1;
    QString clip = trials.args.at(TN(0));
    ui->clipLabel->setText(clip);
}

void Exp::setupNextPlay()
{
    ref = !ref;
    if (ref) ui->trialSpinBox->setValue(DPYTRIALNUM+1); // increment trial
    if (ref) // first, queue up playing ref, wait for nextButton click
    {
        ui->currentLabel->setText("ready to play");
        ui->trialSpinBox->setEnabled(true); // the only time a user should be able to change trial num
#ifdef LOOP_TEST
        //// auto advance /////////////
        QThread::msleep(1000);
        ui->trialSpinBox->setEnabled(false); // the only time a user should be able to change trial num
        ready();
        ui->currentLabel->setText("playing reference");
        dispatch.start();
#endif

    }
    else // just played ref, automatically play comparison
    {
        ui->currentLabel->setText("playing comparison");
        ready();
        dispatch.start();
    }
}

// get trial, set display variables in UI, set bridge from UI
// values are bounded by settings for widgets in UI
void Exp::ready()
{
    trialNumZeroBased = ui->trialSpinBox->value()-1;
    int secs = trials.args.at(TN(1)).toInt();
    QString clip = trials.args.at(TN(0));
    dispatch.setClipAndDur(clip, secs);
    int val = trials.args.at(TN(3)).toInt();
    double fval = trials.args.at(TN(3)).toDouble();
    //    qDebug() << "xxx" << val << trialNumZeroBased;
    if(ref || trials.args.at(TN(2))==QString("ref"))
    {
        ui->delaySpinBox->setValue(0);
        ui->pklossSpinBox->setValue(0);
        ui->driftSpinBox->setValue(0);
        //        qDebug() << "ref";
    }
    else if(trials.args.at(TN(2))==QString("dly"))
    {
        ui->delaySpinBox->setValue(val);
        ui->pklossSpinBox->setValue(0);
        ui->driftSpinBox->setValue(0);
        //        qDebug() << "dly";
    }
    else if(trials.args.at(TN(2))==QString("drift"))
    {
        ui->delaySpinBox->setValue(0);
        ui->pklossSpinBox->setValue(0);
        ui->driftSpinBox->setValue(val);
        //        qDebug() << "drift value has been set";
    }
    else if(trials.args.at(TN(2))==QString("pkloss"))
    {
        ui->delaySpinBox->setValue(0);
        ui->pklossSpinBox->setValue(fval);
        ui->driftSpinBox->setValue(0);
        //        qDebug() << "pkloss";
    }
    if (trials.bridge) dispatch.bridge.erase(&dispatch.state);
    dispatch.state.set_delay(0,ui->delaySpinBox->value());
    dispatch.state.set_loss(0,ui->pklossSpinBox->value());
    dispatch.state.set_drift(0,ui->driftSpinBox->value());
    //    dispatch.state.print();

}
