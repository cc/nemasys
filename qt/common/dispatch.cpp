#include "dispatch.h"
#include <QDebug>
#include "../common/globals.h"

// separate thread for bridge calls and playing clips
Dispatch::Dispatch()
{
    ref = true;
    connect(this, SIGNAL(donePlaying()), &bridge, SLOT(killMplayer()));
}

// fork a thread
void Dispatch::run()
{
    play();
}

// change bridge and play a clip for msecsDur
void Dispatch::play()
{
    bridge.install(&state, true);
//    state.print();
    msleep(msecsDur + CLIPGAP);
    emit donePlaying();
}

// length of time to play a clip
void Dispatch::setClipAndDur(QString clip, int secs)
{
    msecsDur = 1000 * secs;
    bridge.setClip(clip);
}

