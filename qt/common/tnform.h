#ifndef TNFORM_H
#define TNFORM_H

#include <QWidget>
#include "ui_tnform.h"

namespace Ui {
class TNForm;
}

class TNForm : public QWidget
{
    Q_OBJECT

public:
    explicit TNForm(QWidget *parent = 0);
    ~TNForm();

    Ui::TNForm *ui;
};

#endif // TNFORM_H
