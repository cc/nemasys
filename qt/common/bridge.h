#ifndef BRIDGE_H
#define BRIDGE_H

#include <QObject>
#include <QProcess>
#include "../common/state.h"
#include <QDebug>

class Bridge : public QProcess
{
    Q_OBJECT
public:
    Bridge();
    void start();
    void erase(State* state);
    void install(State* state, bool play);
    void setClip(QString cn);
    void setClipPath(QString p) { path = p; }
    void setBridge(bool b) { bridge = b; }
    void setNChans(int n) { nChans = n; }
    void setVideo(bool b) { video = b; }
    void setNIC0(QString str) {_nic0 = str;} // qDebug() << "bridge:setNIC0" << _nic0;}
    void setNIC1(QString str) {_nic1 = str;} // qDebug() << "bridge:setNIC1" << _nic1;}
public slots:
    void killMplayer();
private:
    QString tcArgs(State* state, int which, bool erase);
    int clipNumZerobased;
    QString clip;
    QString path;
    bool bridge;
    int nChans;
    bool video;
    QString _nic0;
    QString _nic1;
};

#endif // BRIDGE_H
