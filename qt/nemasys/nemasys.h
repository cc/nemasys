#ifndef nemasys_H
#define nemasys_H

#include "../common/exp.h"
#include "ui_exp.h"
#include <QWidget>
#include "../common/tnform.h"
#include <QDebug>
#include "../common/bridge.h"
#include "../common/state.h"

class nemasys : public QWidget
{
    Q_OBJECT

public:
    nemasys(QString nic0, QString nic1, QWidget *parent = 0);
    Ui::exp ui;
    TNForm *tnform;
    void setNIC0(QString str) {_nic0 = str;} // qDebug() << "nemasys:" << _nic0;}
    void setNIC1(QString str) {_nic1 = str;} // qDebug() << "nemasys:" << _nic1;}
private slots:
#ifdef EXP
    void on_nextButton_clicked();
    void on_trialSpinBox_valueChanged(int);
#else
    void on_delaySpinBox_valueChanged(int);
    void on_driftSpinBox_valueChanged(int);
    void on_pklossSpinBox_valueChanged(double);
    void on_delaySpinBox_2_valueChanged(int);
    void on_driftSpinBox_2_valueChanged(int);
    void on_pklossSpinBox_2_valueChanged(double);
#endif
protected:
    void  closeEvent(QCloseEvent*);
private:
    Exp exp;
    QString _nic0;
    QString _nic1;
    Bridge *_bridge;
    State *_state;
};

#endif
