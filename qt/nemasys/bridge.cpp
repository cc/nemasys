#include <QDebug>
#include <QThread>
#include "../common/bridge.h"
#include "../common/globals.h"
#include "../common/exp.h"

Bridge::Bridge()
{
    // start(); handled in nemasys.cpp
}

void Bridge::setClip(QString cn)
{
    clip = cn;
}

// create a full tc command to change bridge delay and / or loss
QString Bridge::tcArgs(State* state, int which, bool erase)
{
    // symmetrical, so only using (0)
    QString arguments;
    QTextStream (&arguments) << " tc " << " qdisc " << ((erase)?" del " : " add ")
              << " dev " << ((which)? NIC1:NIC0)
              << " root " << " netem "
              << " delay " << QString::number(state->get_delay(which)) + "ms "
              << QString::number(state->get_drift(which)) + "ms "
              << " loss " << QString::number(state->get_loss(which)) + "% "
                 ;

    //    << QString::number(state->get_drift(0)) + "ms"
    return (arguments);
}

// example tc call:
//    'echo $PATH' tc qdisc add dev eth0 root netem delay 200ms 40ms 25% loss 5% 25% duplicate 1% corrupt 0.1% reorder 5% 50%
// 200ms +- 10ms next random element depending 25% on previous
// 5% of packets lost and each succesive probability depending 25% on previous
// Prob(n)=.25*Prob(n1)+.75*Random

// instantiate a network bridge
void Bridge::start()
{
    QString program;
    QTextStream (&program) << "sudo -S "  << " ifconfig " << NIC0 << " 0.0.0.0 promisc up";
//    qDebug() << program;
    execute(program);

    program.clear();
    QTextStream (&program) << "sudo -S "  << " ifconfig " << NIC1 << " 0.0.0.0 promisc up";
//    qDebug() << program;
    execute(program);
    /*
    ifconfig enp0s20u1 0.0.0.0 promisc up
    ifconfig enp0s20u2 0.0.0.0 promisc up
    ________________________________________________________________________________
    ## bridge might exist, so remove it (a noop if it doesn't, but errors will print)
    ifconfig br0 down
    brctl delbr br0

    ________________________________________________________________________________
    ## add both interfaces to the virtual bridge network
    brctl addbr br0
    brctl setfd br0 0
    brctl addif br0 enp0s20u1
    brctl addif br0 enp0s20u2
    ifconfig br0 up
*/

    qDebug() << "\nfirst, try to delete a previous br0 -- if it doesn't exist ignore the warnings";
    qDebug() << "*******************";
    program.clear();
    QTextStream (&program) << "sudo -S "  << " ifconfig " << " br0 " << "down";
//    qDebug() << program;
    execute(program);

    program.clear();
    QTextStream (&program) << "sudo -S "  << " brctl " << " delbr " << " br0";
//    qDebug() << program;
    execute(program);
    qDebug() << "\n";

    program.clear();
    QTextStream (&program) << "sudo -S "  << " brctl " << " addbr " << " br0";
//    qDebug() << program;
    execute(program);

    program.clear();
    QTextStream (&program) << "sudo -S "  << " brctl " << " setfd " << " br0 " << "0";
//    qDebug() << program;
    execute(program);

    program.clear();
    QTextStream (&program) << "sudo -S "  << " brctl " << " addif " << " br0 " << NIC0;
//    qDebug() << program;
    execute(program);

    program.clear();
    QTextStream (&program) << "sudo -S "  << " brctl " << " addif " << " br0 " << NIC1;
//    qDebug() << program;
    execute(program);

    program.clear();
    QTextStream (&program) << "sudo -S "  << " ifconfig " << " br0 " << "up";
//    qDebug() << program;
    execute(program);

}

// reset bridge state
void Bridge::erase(State* state)
{
    QString program = "sudo -S ";
    program += tcArgs(state,0,true); // true = erase
    execute(program);

    program = "sudo -S ";
    program += tcArgs(state,1,true); // true = erase
    execute(program);
}

// kill mplayer
void Bridge::killMplayer()
{
    QString program = "killall";
    QStringList arguments;
    arguments << "ecasound";
    execute(program, arguments);
    qDebug() << "killall mplayer finished";


    QThread::msleep(333.0);

    //    QString program2 = "killall";
    //    QStringList arguments2;
    //    arguments2 << "chuck";
    //    execute(program2, arguments2);

    QThread::msleep(333.0);

    execute(program, arguments);
    qDebug() << "killall mplayer finished";

    QThread::msleep(333.0);
}

// install a new state, play clip with mplayer connected to jacktrip
void Bridge::install(State* state, bool play)
{
    if (bridge)
    {
        QString program = "sudo -S ";
        program += tcArgs(state,0,false);
//        qDebug() << program;
        execute(program);

        program = "sudo -S ";
        program += tcArgs(state,1,false);
//        qDebug() << program;
        execute(program);
    }

    if (play)
    {
//        QString program2 = MPLAYER;
//        QStringList arg2;
//        if (!video) arg2 <<  "-vo" << "null";
//        arg2
//               <<  "-msglevel" << "all=1"
//                    //                <<  "-ao" << "jack:name=xxx"
//                 <<  "-ao" << "jack:port=JackTrip"
//                  << "-channels" << "6" // plays ok in stereo without QString::number(nChans)
//                  << "-af" << "channels=6:6:0:0:1:1:2:4:3:5:4:2:5:3"
//                  << "-xineramascreen" << "1"
//                  << "-fs"
//                  << path + clip;
//        // example path        << "/home/cc/Desktop/MKV_examples/Mission_Impossible_1.mkv";
//        //qDebug() << program2 << arg2;
//        startDetached(program2, arg2);

        // this was unused
        //        QString program3 = "jack_connect";
        //        QStringList arg3;
        //        for (int i=0; i<nChans; i++)
        //        {
        //            arg3.clear();
        //            arg3 <<  QString("xxx:out_")+QString::number(i) << QString("JackTrip:send_")+
        //                     QString::number((i%nChans)+1);
        //            qDebug() << program3 << arg3;
        //            execute(program3, arg3);
        //        }

        QString program2 =  "ecasound -i /home/cc/Desktop/brir/monoSmSm.wav -o jack,JackTrip ";
        QStringList arg2;
//        if (!video) program2 <<
        // example path        << "/home/cc/Desktop/MKV_examples/Mission_Impossible_1.mkv";
        qDebug() << program2 << arg2;
        startDetached(program2);
    }
}

