# super simple .pro file maintained by Qt creator
HEADERS     = nemasys.h \
    trials.h \
    ../common/globals.h \
    ../common/exp.h \
    ../common/globals.h \
    ../common/dispatch.h \
    ../common/state.h \
    ../common/bridge.h \
    ../common/tnform.h

FORMS       = \
    ../common/exp.ui \
    ../common/tnform.ui

SOURCES     = nemasys.cpp \
              main.cpp \
    bridge.cpp \
    ../common/exp.cpp \
    ../common/dispatch.cpp \
    ../common/state.cpp \
    ../common/tnform.cpp
QT += widgets

#target.path = $$[QT_INSTALL_EXAMPLES]/designer/nemasys
#INSTALLS += target

#DISTFILES +=
