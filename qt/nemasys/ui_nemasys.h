/********************************************************************************
** Form generated from reading UI file 'nemasys.ui'
**
** Created by: Qt User Interface Compiler version 5.4.2
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_EXP2_H
#define UI_EXP2_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QDoubleSpinBox>
#include <QtWidgets/QFrame>
#include <QtWidgets/QGridLayout>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLabel>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QSpacerItem>
#include <QtWidgets/QSpinBox>
#include <QtWidgets/QVBoxLayout>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_nemasys
{
public:
    QGridLayout *gridLayout;
    QVBoxLayout *vboxLayout;
    QLabel *label;
    QSpinBox *delaySpinBox;
    QSpacerItem *horizontalSpacer;
    QSpacerItem *verticalSpacer;
    QSpinBox *trialSpinBox;
    QLabel *clipLabel;
    QFrame *line;
    QSpacerItem *verticalSpacer_2;
    QVBoxLayout *verticalLayout;
    QLabel *label_3;
    QDoubleSpinBox *pklossSpinBox;
    QSpacerItem *verticalSpacer_3;
    QSpacerItem *verticalSpacer_4;
    QVBoxLayout *verticalLayout_2;
    QLabel *label_4;
    QSpinBox *driftSpinBox;
    QLabel *currentLabel;
    QFrame *line_2;
    QLabel *label_2;
    QLabel *label_5;
    QPushButton *nextButton;

    void setupUi(QWidget *nemasys)
    {
        if (nemasys->objectName().isEmpty())
            nemasys->setObjectName(QStringLiteral("nemasys"));
        nemasys->resize(388, 297);
        QSizePolicy sizePolicy(QSizePolicy::Preferred, QSizePolicy::Preferred);
        sizePolicy.setHorizontalStretch(0);
        sizePolicy.setVerticalStretch(0);
        sizePolicy.setHeightForWidth(nemasys->sizePolicy().hasHeightForWidth());
        nemasys->setSizePolicy(sizePolicy);
        gridLayout = new QGridLayout(nemasys);
        gridLayout->setSpacing(6);
        gridLayout->setObjectName(QStringLiteral("gridLayout"));
        gridLayout->setContentsMargins(9, 9, 9, 9);
        vboxLayout = new QVBoxLayout();
        vboxLayout->setSpacing(6);
        vboxLayout->setObjectName(QStringLiteral("vboxLayout"));
        vboxLayout->setContentsMargins(1, 1, 1, 1);
        label = new QLabel(nemasys);
        label->setObjectName(QStringLiteral("label"));

        vboxLayout->addWidget(label);

        delaySpinBox = new QSpinBox(nemasys);
        delaySpinBox->setObjectName(QStringLiteral("delaySpinBox"));
        delaySpinBox->setMaximum(400);

        vboxLayout->addWidget(delaySpinBox);


        gridLayout->addLayout(vboxLayout, 1, 0, 2, 1);

        horizontalSpacer = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        gridLayout->addItem(horizontalSpacer, 1, 4, 1, 1);

        verticalSpacer = new QSpacerItem(20, 40, QSizePolicy::Minimum, QSizePolicy::Expanding);

        gridLayout->addItem(verticalSpacer, 11, 2, 1, 1);

        trialSpinBox = new QSpinBox(nemasys);
        trialSpinBox->setObjectName(QStringLiteral("trialSpinBox"));
        trialSpinBox->setMinimum(1);
        trialSpinBox->setValue(99);

        gridLayout->addWidget(trialSpinBox, 7, 2, 1, 1);

        clipLabel = new QLabel(nemasys);
        clipLabel->setObjectName(QStringLiteral("clipLabel"));
        clipLabel->setAlignment(Qt::AlignCenter);

        gridLayout->addWidget(clipLabel, 10, 2, 1, 1);

        line = new QFrame(nemasys);
        line->setObjectName(QStringLiteral("line"));
        line->setFrameShape(QFrame::HLine);
        line->setFrameShadow(QFrame::Sunken);

        gridLayout->addWidget(line, 6, 0, 1, 4);

        verticalSpacer_2 = new QSpacerItem(20, 40, QSizePolicy::Minimum, QSizePolicy::Expanding);

        gridLayout->addItem(verticalSpacer_2, 3, 0, 1, 1);

        verticalLayout = new QVBoxLayout();
        verticalLayout->setObjectName(QStringLiteral("verticalLayout"));
        label_3 = new QLabel(nemasys);
        label_3->setObjectName(QStringLiteral("label_3"));

        verticalLayout->addWidget(label_3);

        pklossSpinBox = new QDoubleSpinBox(nemasys);
        pklossSpinBox->setObjectName(QStringLiteral("pklossSpinBox"));

        verticalLayout->addWidget(pklossSpinBox);


        gridLayout->addLayout(verticalLayout, 1, 2, 2, 1);

        verticalSpacer_3 = new QSpacerItem(20, 40, QSizePolicy::Minimum, QSizePolicy::Expanding);

        gridLayout->addItem(verticalSpacer_3, 3, 2, 1, 1);

        verticalSpacer_4 = new QSpacerItem(20, 40, QSizePolicy::Minimum, QSizePolicy::Expanding);

        gridLayout->addItem(verticalSpacer_4, 3, 1, 1, 1);

        verticalLayout_2 = new QVBoxLayout();
        verticalLayout_2->setObjectName(QStringLiteral("verticalLayout_2"));
        label_4 = new QLabel(nemasys);
        label_4->setObjectName(QStringLiteral("label_4"));

        verticalLayout_2->addWidget(label_4);

        driftSpinBox = new QSpinBox(nemasys);
        driftSpinBox->setObjectName(QStringLiteral("driftSpinBox"));
        driftSpinBox->setMaximum(30);

        verticalLayout_2->addWidget(driftSpinBox);


        gridLayout->addLayout(verticalLayout_2, 1, 1, 2, 1);

        currentLabel = new QLabel(nemasys);
        currentLabel->setObjectName(QStringLiteral("currentLabel"));

        gridLayout->addWidget(currentLabel, 3, 4, 2, 1);

        line_2 = new QFrame(nemasys);
        line_2->setObjectName(QStringLiteral("line_2"));
        line_2->setFrameShape(QFrame::VLine);
        line_2->setFrameShadow(QFrame::Sunken);

        gridLayout->addWidget(line_2, 1, 3, 3, 1);

        label_2 = new QLabel(nemasys);
        label_2->setObjectName(QStringLiteral("label_2"));

        gridLayout->addWidget(label_2, 7, 4, 1, 1);

        label_5 = new QLabel(nemasys);
        label_5->setObjectName(QStringLiteral("label_5"));
        label_5->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);

        gridLayout->addWidget(label_5, 7, 1, 1, 1);

        nextButton = new QPushButton(nemasys);
        nextButton->setObjectName(QStringLiteral("nextButton"));

        gridLayout->addWidget(nextButton, 10, 4, 1, 1);


        retranslateUi(nemasys);

        QMetaObject::connectSlotsByName(nemasys);
    } // setupUi

    void retranslateUi(QWidget *nemasys)
    {
        nemasys->setWindowTitle(QApplication::translate("nemasys", "bridge", 0));
        label->setText(QApplication::translate("nemasys", "delay", 0));
        clipLabel->setText(QApplication::translate("nemasys", "clip", 0));
        label_3->setText(QApplication::translate("nemasys", "pkloss", 0));
        label_4->setText(QApplication::translate("nemasys", "drift", 0));
        currentLabel->setText(QApplication::translate("nemasys", "ready", 0));
        label_2->setText(QApplication::translate("nemasys", "is next", 0));
        label_5->setText(QApplication::translate("nemasys", "trial", 0));
        nextButton->setText(QApplication::translate("nemasys", "go", 0));
    } // retranslateUi

};

namespace Ui {
    class nemasys: public Ui_nemasys {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_EXP2_H
