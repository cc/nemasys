#include <QApplication>
#include <QDebug>
#include <QLoggingCategory>
#include <QCommandLineParser>
#include <QCommandLineOption>
#include <QMessageBox>

#include "nemasys.h"
#include "../common/globals.h"

void myMessageOutput(QtMsgType type, const QMessageLogContext &context, const QString &msg)
{
    (void)type;
    (void)context;
    QByteArray localMsg = msg.toLocal8Bit();
    fprintf(stderr, "%s\n", localMsg.constData());
    fflush(stderr);
}

int main(int argc, char *argv[])
{
    QApplication app(argc, argv);
    app.setApplicationVersion(VERSION);

    // needed for qDebug() printing
    QLoggingCategory::setFilterRules("*.debug=true\nqt.*.debug=false");
    qInstallMessageHandler(myMessageOutput);

    // command line stuff
    QCommandLineParser parser;
    parser.setApplicationDescription("ethernet network bridge\nversion "
                                     VERSION
                                     " asymmetric (2 NICS, 2 sets of controls)\n"
                                     "start me with a script that detects and supplies NIC names\n"
                                     "(unless those are known in advance)");
    parser.addHelpOption();
    parser.addVersionOption();

    // add cmd lind option -a --nicA named NICA
    QCommandLineOption NICA(QStringList() << "a" << "nicA",
                            QCoreApplication::translate("main",
                                                        "attach <nicA> to ether bridge."),
                            QCoreApplication::translate("main",
                                                        "nicA"));
    parser.addOption(NICA);

    // -b --nicB
    QCommandLineOption NICB(QStringList() << "b" << "nicB",
                            QCoreApplication::translate("main",
                                                        "attach <nicB> to ether bridge."),
                            QCoreApplication::translate("main",
                                                        "nicB"));
    parser.addOption(NICB);

    parser.process(app);
    QString dummy0("dummy0");
    QString dummy1("dummy1");
    QString nicA = (parser.value(NICA).isNull())?"dummy0":parser.value(NICA);
    QString nicB = (parser.value(NICB).isNull())?"dummy1":parser.value(NICB);

    // error detected but keep going after user acknowledges it
    if ( (nicA == dummy0) || (nicB == dummy1) ) {
        QMessageBox msgBox;
        msgBox.setText("One or more NICs missing from argument list");
        msgBox.exec();
    }

    qDebug() << "creating bridge br0 with NICs set to:" << nicA << "and" << nicB;
    // instantiate bridge and show UI
    nemasys ui(nicA,nicB);
    ui.show();
    return app.exec();
}

