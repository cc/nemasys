#include <QtGui>
#include <QDebug>
#include "nemasys.h"
#include "../common/globals.h"

/*
 * the GUI appears on start up
 * code related to running EXP is for automatic trials
 *   used in running listening tests
 * but is unused in simple bridge VERSION 1.0.0 and higher
*/

nemasys::nemasys(QString nic0, QString nic1, QWidget *parent)
    :
      QWidget(parent),
      _nic0(nic0),
      _nic1(nic1)
{
    ui.setupUi(this);
    exp.ui = &ui; // bridge controller
    tnform = new TNForm(); // unused EXP

#ifdef EXP
    tnform->show();
#else
    // mask off items used only for EXP
    ui.clipLabel->hide();
    ui.currentLabel->hide();
    ui.nextButton->hide();
    ui.trialSpinBox->hide();
    ui.label_2->hide();
    ui.label_5->hide();

    // tailor for simple bridge with impairments
    // hijack "drift" controls and use for jitter
    ui.label_4->setText(QString("jitter")); // was "drift"
    ui.label_7->setText(QString("jitter")); // was "drift"
    this->setGeometry(QRect(150,150,150,150));

    // grab bridge and state from EXP modules
    _bridge = &exp.dispatch.bridge;
    _state = &exp.dispatch.state;

    // allow bridge to run
    _bridge->setBridge(true);

    // set NICs from cmd line
    _bridge->setNIC0(_nic0);
    _bridge->setNIC1(_nic1);
    if (_nic0 != QString ("dummy0"))
        ui.label_9->setText(_nic0);
    if (_nic1 != QString ("dummy1"))
        ui.label_10->setText(_nic1);

    // emit commands that configure and bring up the bridge
    _bridge->start();

    // emit tc commands that install the default state into the bridge
    _bridge->install(_state, false); // don't play media locally

    // print initial state
    _state->print();
#endif

#ifdef EXP
    tnform->ui->tnLabel->setText(QString::number(ui.trialSpinBox->value()));
#include "trials.h" // input Jimmy's list of trials
    exp.go();
#endif
}

#ifndef EXP

// 3 controls: delay, jitter, packet loss
// on user change to one of these values
// emit tc commands to erase the existing state of bridge
// create the new state by setting the new value
// emit tc commands that install the new state into the bridge
// don't play any media locally because this is just bridge control

void nemasys::on_delaySpinBox_valueChanged(int)
{
    //    qDebug() << ui.delaySpinBox->value();
    _bridge->erase(_state);
    _state->set_delay(0,ui.delaySpinBox->value());
    _bridge->install(_state, false);
    _state->print();
}

// "drift" control has been hijacked for jitter
void nemasys::on_driftSpinBox_valueChanged(int)
{
    //    qDebug() << ui.driftSpinBox->value();
    _bridge->erase(_state);
    _state->set_drift(0,ui.driftSpinBox->value());
    _bridge->install(_state, false);
    _state->print();
}

void nemasys::on_pklossSpinBox_valueChanged(double)
{
    //    qDebug() << ui.pklossSpinBox->value();
    _bridge->erase(_state);
    _state->set_loss(0,ui.pklossSpinBox->value());
    _bridge->install(_state, false);
    _state->print();
}

///////////////////////////////
// second set of controls for asymmetrical

void nemasys::on_delaySpinBox_2_valueChanged(int)
{
    //    qDebug() << ui.delaySpinBox_2->value();
    _bridge->erase(_state);
    _state->set_delay(1,ui.delaySpinBox_2->value());
    _bridge->install(_state, false);
    _state->print();
}

// "drift" control has been hijacked for jitter
void nemasys::on_driftSpinBox_2_valueChanged(int)
{
    //    qDebug() << ui.driftSpinBox_2->value();
    _bridge->erase(_state);
    _state->set_drift(1,ui.driftSpinBox_2->value());
    _bridge->install(_state, false);
    _state->print();
}

void nemasys::on_pklossSpinBox_2_valueChanged(double)
{
    //    qDebug() << ui.pklossSpinBox_2->value();
    _bridge->erase(_state);
    _state->set_loss(1,ui.pklossSpinBox_2->value());
    _bridge->install(_state, false);
    _state->print();
}
#endif

void   nemasys::closeEvent(QCloseEvent*)
{
    qApp->quit();
}

#ifdef EXP
void nemasys::on_trialSpinBox_valueChanged(int)
{
    exp.trialNumChanged();
    tnform->ui->tnLabel->setText(QString::number(ui.trialSpinBox->value()));
}

// play ref
void nemasys::on_nextButton_clicked()
{
    ui.trialSpinBox->setEnabled(false);
    exp.ready();
    ui.currentLabel->setText("playing reference");
    exp.dispatch.start();
    // will automatically play comparison
}
#endif
