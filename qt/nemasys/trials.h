#ifndef TRIALS_H
#define TRIALS_H
#include "../common/globals.h"
/////////////////////////
// this isn't really a .h -- more like a settings file
// these settings are for Exp2
/////////////////////////

// network bridge
// experiment 1 = true, 2 = true, 3 = false
exp.trials.bridge = true;

// audio output and path full path to where the clips and stems live
#ifndef AT_GIBSON
// experiment 1 = 2, 2 = 6, 3 = 6
exp.trials.nChans = 2;
exp.trials.path = QString("/home/cc/git-etherbridge/clips/Exp2Clips/");
#else
// experiment 1 = 2, 2 = 6, 3 = 6
exp.trials.nChans = 6;
exp.trials.path = QString("/home/cc/git-etherbridge/clips/Exp2Clips/");
#endif

// with video output
// experiment 1 = true, 2 = false, 3 = false
exp.trials.video = false;


//xxxxxxxxxxxxxxxxxxxxx next entry is an HM not a CB ----- wrong name
exp.trials.args
//<< "HM_Clip4.wav" << QString::number(17) << "pkloss" << QString::number(0.4)
<< "CB_Clip1.wav" << QString::number(16) << "ref" << QString::number(0)
<< "CB_Clip3.wav" << QString::number(14) << "pkloss" << QString::number(0.05)
<< "HM_Clip1.wav" << QString::number(10) << "ref" << QString::number(0)
<< "CB_Clip2.wav" << QString::number(10) << "pkloss" << QString::number(0.05)
<< "HM_Clip2.wav" << QString::number(11) << "ref" << QString::number(0)
<< "CB_Clip1.wav" << QString::number(16) << "pkloss" << QString::number(0.02)
<< "HM_Clip1.wav" << QString::number(10) << "ref" << QString::number(0)
<< "HM_Clip1.wav" << QString::number(10) << "pkloss" << QString::number(0.05)
<< "CB_Clip3.wav" << QString::number(14) << "pkloss" << QString::number(0.02)
<< "CB_Clip1.wav" << QString::number(16) << "ref" << QString::number(0)
<< "CB_Clip2.wav" << QString::number(10) << "pkloss" << QString::number(0.02)
<< "HM_Clip1.wav" << QString::number(10) << "pkloss" << QString::number(0.05)
<< "HM_Clip2.wav" << QString::number(11) << "pkloss" << QString::number(0.05)
<< "HM_Clip1.wav" << QString::number(10) << "pkloss" << QString::number(0.02)
<< "CB_Clip3.wav" << QString::number(14) << "pkloss" << QString::number(0.02)
<< "CB_Clip3.wav" << QString::number(14) << "ref" << QString::number(0)
<< "HM_Clip2.wav" << QString::number(11) << "pkloss" << QString::number(0.02)
<< "CB_Clip3.wav" << QString::number(14) << "ref" << QString::number(0)
<< "HM_Clip2.wav" << QString::number(11) << "pkloss" << QString::number(0.05)
<< "CB_Clip2.wav" << QString::number(10) << "pkloss" << QString::number(0.05)
<< "CB_Clip1.wav" << QString::number(16) << "pkloss" << QString::number(0.05)
<< "CB_Clip2.wav" << QString::number(10) << "ref" << QString::number(0)
<< "HM_Clip2.wav" << QString::number(11) << "pkloss" << QString::number(0.02)
<< "CB_Clip2.wav" << QString::number(10) << "pkloss" << QString::number(0.02)
<< "CB_Clip1.wav" << QString::number(16) << "pkloss" << QString::number(0.05)
<< "CB_Clip3.wav" << QString::number(14) << "pkloss" << QString::number(0.05)
<< "HM_Clip2.wav" << QString::number(11) << "ref" << QString::number(0)
<< "CB_Clip2.wav" << QString::number(10) << "ref" << QString::number(0)
<< "HM_Clip1.wav" << QString::number(10) << "pkloss" << QString::number(0.02)
<< "CB_Clip1.wav" << QString::number(16) << "pkloss" << QString::number(0.02)
;
#endif // TRIALS_H
